package kits.my;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.hutool.core.util.ReUtil;
import jodd.io.FileUtil;
import jodd.util.StringUtil;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FanyiKit {
	private @NonNull String path;

//	private final String pattern = "([^\\x00-\\xff]+)";
	private final String pattern = "([\\u4e00-\\u9fa5]+)";
	private final String patternFyEd = "\\$\\{L.*?\\)}";
	
	
	public void replace() throws IOException {
		String html = FileUtil.readString(path);
		List<String> resultFindAll = ReUtil.findAll(patternFyEd, html, 0, new ArrayList<String>());
		for (String patt : resultFindAll) {
			String temp = patt.replace("${L:l(lan,'", "");
			temp = temp.replace("')}", "");
			html = StringUtil.replace(html, patt, temp);
		}
		String replaceAllFy = ReUtil.replaceAll(html, pattern, "${L:l(lan,'$1')}");
		System.out.println(replaceAllFy);
		
		
	}
	
	



	public static void main(String[] args) throws IOException {
		FanyiKit fanyiKit = new FanyiKit("E:\\workspace_compter\\zb_main\\src\\main\\webapp\\cn\\eosvote\\index.jsp");
		fanyiKit.replace();
	}
}
