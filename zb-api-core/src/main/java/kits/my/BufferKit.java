package kits.my;

import cn.hutool.core.util.StrUtil;
import jodd.buffer.FastCharBuffer;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Administrator
 *
 */
@NoArgsConstructor
public class BufferKit {
	private FastCharBuffer fb =  new FastCharBuffer();

	public BufferKit append(String str) {
		fb.append(str+"\n");
		return this;
	}
	
	public BufferKit append(CharSequence template, Object... params) {
		return this.append(StrUtil.format(template, params));
	}
	
	public BufferKit appendLast(String str) {
		fb.append(str);
		return this;
	}
	
	public BufferKit appendLast(CharSequence template, Object... params) {
		return this.appendLast(StrUtil.format(template, params));
	}

	@Override
	public String toString() {
		return fb.toString();
	}
	
	
}
