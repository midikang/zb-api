## 获取杠杆账单
```request
GET https://trade.zb.cn/api/getLeverBills?accesskey=youraccesskey&coin=qc&method=getLeverBills&pageIndex=1&pageSize=10&sign=请求加密签名串&reqTime=当前时间毫秒数
```
```response
{
	"code"              : 1000,
	"message"           : "操作成功。",
	"result"            : [
		{
			"changeCoin"    : 500.00000000,
			"showCoin"      : "+500.000=500.000LTC",
			"type"          : 1,
			"date"          : 1522668146000,
			"database"      : "default",
			"dataName"      : "baseBean",
			"fundsType"     : 3,
			"billTypeValue" : "LTC转入",
			"id"            : 609,
			"downTableDate" : 1520092800000,
			"marketName"    : "ltcusdt",
			"avgPrice"      : 0,
			"userId"        : 110803,
			"coinBalance"   : 500.00000000,
			"entrustId"     : 0
		}
	]
}
```
**请求参数**

参数名称 | 类型 | 取值范围
---|---|---
method | String | 直接赋值 getLeverBills
accesskey | String | accesskey
coin | String | 币种
dataType | int | 数据类型[0/1]（30天内数据/30天前数据）
pageIndex | int | 当前页数
pageSize | int | 每页数量
sign | String | 请求加密签名串
reqTime | long | 当前时间毫秒数

```resdata
code : 返回代码
message : 提示信息
result :
        id            : 账单ID
        userId        : 用户ID
        date          : 账单生成时间
        type          : 账单类型
        billTypeValue : 账单类型对应中文
        changeCoin    : 本次变化的COIN数量
        coinBalance   : 变化后的COIN余额
        changeFiat    : 本次变化的法币数量
        fiatBalance   : 变化后的法币余额
        fundsType     : COIN类型
        marketName    : 市场名称
        showCoin      : 显示COIN变化
        showFiat      : 显示法币变化
```

```java
public void getLeverBills() {
	//测试apiKey:ce2a18e0-dshs-4c44-4515-9aca67dd706e
	//测试secretKey:c11a122s-dshs-shsa-4515-954a67dd706e
	//加密类:https://github.com/zb2017/api/blob/master/zb_netty_client_java/src/main/java/websocketx/client/EncryDigestUtil.java
	//secretKey通过sha1加密得到:86429c69799d3d6ac5da5c2c514baa874d75a4ba
	String digest = EncryDigestUtil.digest("c11a122s-dshs-shsa-4515-954a67dd706e");
	//参数按照ASCII值排序
	String paramStr = "accesskey=ce2a18e0-dshs-4c44-4515-9aca67dd706e&coin=qc&dataType=0&method=getLeverBills&pageIndex=1&pageSize=10";
	//sign通过HmacMD5加密得到:2473a00d93feed72583365e3cb90f391
	String sign = EncryDigestUtil.hmacSign(paramStr, digest);
	//组装最终发送的请求url
	String finalUrl = "https://trade.zb.cn/api/getLeverBills?accesskey=ce2a18e0-dshs-4c44-4515-9aca67dd706e&coin=qc&dataType=0&method=getLeverBills&pageIndex=1&pageSize=10&reqTime=1539942326070&sign=2473a00d93feed72583365e3cb90f391";
	//取得返回结果json
	String resultJson = HttpRequest.get(finalUrl).send().bodyText();
}
```

```python
import hashlib
import struct
import time
import requests

class ZApi:
    def __init__(self, access_key, secret_key):
        self._access_key_ = access_key
        self._secret_key_ = secret_key
        self._markets_ = self.markets()
        if len(self._markets_) < 1:
            raise Exception("Get markets status failed")

    def getLeverBills(self, market, type, amount, price):
        MyUrl = 'https://trade.zb.cn/api/getLeverBills'
        params = 'accesskey=ce2a18e0-dshs-4c44-4515-9aca67dd706e&coin=qc&dataType=0&method=getLeverBills&pageIndex=1&pageSize=10'
        return self.call_api(url=MyUrl, params=params)

    def call_api(self, url, params=''):
        full_url = url
        if params:
            #sha_secret=86429c69799d3d6ac5da5c2c514baa874d75a4ba
            sha_secret = self.digest(self._secret_key_)
            #sign = 2473a00d93feed72583365e3cb90f391
            sign = self.hmac_sign(params, sha_secret)
            req_time = int(round(time.time() * 1000))
            params += '&sign=%s&reqTime=%d' % (sign, req_time)
            #full_url = https://trade.zb.cn/api/getLeverBills?accesskey=ce2a18e0-dshs-4c44-4515-9aca67dd706e&coin=qc&dataType=0&method=getLeverBills&pageIndex=1&pageSize=10&reqTime=1540295995560&sign=2473a00d93feed72583365e3cb90f391
            full_url += '?' + params
        result = {}
        while True:
            try:
                r = requests.get(full_url, timeout=2)
            except Exception:
                time.sleep(0.5)
                continue
            if r.status_code != 200:
                time.sleep(0.5)
                r.close()
                continue
            else:
                result = r.json()
                r.close()
                break
        return result

    @staticmethod
    def fill(value, lenght, fill_byte):
        if len(value) >= lenght:
            return value
        else:
            fill_size = lenght - len(value)
        return value + chr(fill_byte) * fill_size

    @staticmethod
    def xor(s, value):
        slist = list(s.decode('utf-8'))
        for index in range(len(slist)):
            slist[index] = chr(ord(slist[index]) ^ value)
        return "".join(slist)

    def hmac_sign(self, arg_value, arg_key):
        keyb = struct.pack("%ds" % len(arg_key), arg_key.encode('utf-8'))
        value = struct.pack("%ds" % len(arg_value), arg_value.encode('utf-8'))
        k_ipad = self.xor(keyb, 0x36)
        k_opad = self.xor(keyb, 0x5c)
        k_ipad = self.fill(k_ipad, 64, 54)
        k_opad = self.fill(k_opad, 64, 92)
        m = hashlib.md5()
        m.update(k_ipad.encode('utf-8'))
        m.update(value)
        dg = m.digest()

        m = hashlib.md5()
        m.update(k_opad.encode('utf-8'))
        subStr = dg[0:16]
        m.update(subStr)
        dg = m.hexdigest()
        return dg
  
    def digest(self, arg_value):
        value = struct.pack("%ds" % len(arg_value), arg_value.encode('utf-8'))
        h = hashlib.sha1()
        h.update(value)
        dg = h.hexdigest()
        return dg
```