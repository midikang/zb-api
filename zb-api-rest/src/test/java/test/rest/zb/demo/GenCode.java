package test.rest.zb.demo;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import entity.commons.UserApi;
import entity.enumtype.ExchangeEnum;
import entity.enumtype.TradeEnum;
import rest.zb.rest.api.RestApiPost;

public class GenCode {
	private RestApiPost api;
	private final String apiKey = "ce2a18e0-dshs-4c44-4515-9aca67dd706e";
	private final String secretKey = "c11a122s-dshs-shsa-4515-954a67dd706e";
	
	@Test
	public void test() {
		api.getAccount();
		api.buy(1, 0.001);
		api.getOrder(20180522105585216l);
		api.cancelOrder(20180522105585216l);
		api.getOrdersIgnoreTradeType();
		
		api.getUserAddress();
		api.getWithdrawAddress();
		api.getOrders(TradeEnum.buy, 1);
		api.getUnfinishedOrdersIgnoreTradeType(1);
		api.getWithdrawRecord();
		api.withdraw(1.5, 0.05, "地址string");
		api.getLeverAssetsInfo();
		api.getLeverBills();
		api.transferInLever();
		api.transferOutLever();
		api.loan();
		api.cancelLoan();
		api.getLoans();
		api.getLoanRecords();
		api.borrow();
		api.repay();
		api.getRepayments();
		api.getFinanceRecords();
		api.getChargeRecord();
		api.changeInvestMark();
		api.changeLoop();
		
		
		api.addSubUser();
		api.createSubUserKey();
		api.getSubUserList();
		api.doTransferFunds();
	}
	
	
	@Before
	public void init() throws IOException {
		String symbol = "bch_qc";
		String url = "https://trade.zb.cn/api/";
		// 构造交易接口对象
		UserApi userApi = new UserApi(ExchangeEnum.zb, "测试", apiKey, secretKey);
		userApi.setPayPass("123456bb");
		api = new RestApiPost(url, symbol, userApi);

	}
}
